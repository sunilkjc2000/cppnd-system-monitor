#include <unistd.h>
#include <cstddef>
#include <set>
#include <string>
#include <vector>
#include "linux_parser.h"
#include "process.h"
#include "processor.h"
#include "system.h"

using std::set;
using std::size_t;
using std::string;
using std::vector;

// TODO: Return the system's CPU
Processor& System::Cpu() { return cpu_; }

// TODO: Return a container composed of the system's processes
vector<Process>& System::Processes() 
{ 
  Process proc;
  float   utilization = 0.0;
  vector<int> pids = LinuxParser::Pids();
  processes_.clear();
  for(int procidx : pids)
  {
    
    proc.setProcId(procidx);
    proc.setCommand(LinuxParser::Command(procidx));
    proc.setUsrName(LinuxParser::User(procidx));
    //Process active time/(system uptime - start time)
    utilization = (((float)LinuxParser::ActiveJiffies(procidx))/(float)  
                   (LinuxParser::Jiffies()/sysconf(_SC_CLK_TCK)))*100;
    proc.setUtilization(utilization);
    proc.setRam(LinuxParser::Ram(procidx));
    // Process up time = system uptime - process start time
    proc.setUpTime(LinuxParser::UpTime() - LinuxParser::UpTime(procidx));
  
    processes_.push_back(proc);
  }  
  return processes_; 
}

// TODO: Return the system's kernel identifier (string)
std::string System::Kernel() { return LinuxParser::Kernel(); }

// TODO: Return the system's memory utilization
float System::MemoryUtilization() 
{ return LinuxParser::MemoryUtilization(); }

// TODO: Return the operating system name
std::string System::OperatingSystem() 
{ return LinuxParser::OperatingSystem(); }

// TODO: Return the number of processes actively running on the system
int System::RunningProcesses() 
{ return LinuxParser::RunningProcesses(); }

// TODO: Return the total number of processes on the system
int System::TotalProcesses() 
{ return LinuxParser::TotalProcesses(); }

// TODO: Return the number of seconds since the system started running
long int System::UpTime() { return LinuxParser::UpTime(); }