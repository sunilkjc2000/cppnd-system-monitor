#include <string>
#include <sstream>
#include "format.h"

using std::string;

// TODO: Complete this helper function
// INPUT: Long int measuring seconds
// OUTPUT: HH:MM:SS
// REMOVE: [[maybe_unused]] once you define the function
std::string Format::longToString(long val)
{
  std::stringstream convert;
  convert << val;
  return(convert.str());
}
std::string Format::ElapsedTime(long seconds) 
{ 
  long HH{0},MM{0},SEC{0},RemainingSecs{0};
  HH = seconds/(60*60);
  RemainingSecs = seconds - HH*(60*60);
  MM = RemainingSecs/60;
  RemainingSecs = RemainingSecs - MM*60;
  SEC = RemainingSecs;
  
  std::string Result;
  Result = longToString(HH) + ":" + longToString(MM) + ":" +                longToString(SEC);
  return Result; 
}