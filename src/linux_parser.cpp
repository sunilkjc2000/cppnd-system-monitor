#include <dirent.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <sstream>

#include "linux_parser.h"

using std::stof;
using std::string;
using std::to_string;
using std::vector;
using std::stringstream;

long  LinuxParser::idleJiffies = 0;
long  LinuxParser::totalJiffies = 0;
// DONE: An example of how to read data from the filesystem
string LinuxParser::OperatingSystem() {
  string line{""};
  string key{""};
  string value{""};
  std::ifstream filestream(kOSPath);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::replace(line.begin(), line.end(), ' ', '_');
      std::replace(line.begin(), line.end(), '=', ' ');
      std::replace(line.begin(), line.end(), '"', ' ');
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "PRETTY_NAME") {
          std::replace(value.begin(), value.end(), '_', ' ');
          return value;
        }
      }
    }
    filestream.close();
  }
  return value;
}

// DONE: An example of how to read data from the filesystem
string LinuxParser::Kernel() {
  string os{""}, kernel{""};
  string line{""};
  std::ifstream stream(kProcDirectory + kVersionFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> os >> kernel;
    stream.close();
    return kernel;
  }
  else
  {
    return string();
  }
  
}

// BONUS: Update this to use std::filesystem
vector<int> LinuxParser::Pids() {
  vector<int> pids;
  DIR* directory = opendir(kProcDirectory.c_str());
  struct dirent* file;
  while ((file = readdir(directory)) != nullptr) {
    // Is this a directory?
    if (file->d_type == DT_DIR) {
      // Is every character of the name a digit?
      string filename(file->d_name);
      if (std::all_of(filename.begin(), filename.end(), isdigit)) {
        int pid = stoi(filename);
        pids.push_back(pid);
      }
    }
  }
  closedir(directory);
  return pids;
}

// TODO: Read and return the system memory utilization
float LinuxParser::MemoryUtilization() 
{ 
  string line{""};
  string key{""};
  string value{""};
  string units{""};
  float memoryUtilization = 0,totMem = 0; 
  std::ifstream filestream(kProcDirectory + kMeminfoFilename);
  if (filestream.is_open()) {
    
    while (std::getline(filestream, line)) {
      std::replace(line.begin(), line.end(), ':', ' ');
      std::istringstream linestream(line);
      linestream >> key >> value >> units;
      {
        if (key == "MemTotal") {
          memoryUtilization = stof(value);
          totMem = totMem + stof(value);
        }
        else if(key == "MemFree")
        {
          totMem = totMem + stof(value);
          if(memoryUtilization > stof(value))
          {
            memoryUtilization = memoryUtilization - stof(value);
          }
          else
          {
            memoryUtilization = stof(value) -memoryUtilization;
          }
         
          
        }
        else if(key == "MemAvailable")
        {
          totMem = totMem + stof(value);
          break;
        }
      } 
    }
    filestream.close();
    return ((memoryUtilization/totMem)) ;
  }
  else
  {
    return 0.0;
  }
}

// TODO: Read and return the system uptime
long LinuxParser::UpTime() 
{  
  string cpu_uptime{""}, cpu_idletime{""};
  string line{""};
  std::ifstream stream(kProcDirectory + kUptimeFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> cpu_uptime >> cpu_idletime;
    stream.close();
    return stol(cpu_uptime);
  }
  else
  {
    return 0;
  }
}

// TODO: Read and return the number of jiffies for the system
long LinuxParser::Jiffies() 
{ 
  
  string line{""},Jiffie{""};
  int    idx = 0;
  idleJiffies = 0;
  totalJiffies = 0;
  std::ifstream stream(kProcDirectory + kStatFilename);
  // First line of stat files contains system jiffies info
  // adding up all jiffies here
  if (stream.is_open()) 
  {
    std::getline(stream, line);
    std::stringstream linestream(line);
    while (linestream.good())
    {
       //skip first string
       if(idx != 0) 
       {
         linestream >> Jiffie;
         // Donot include Guest and Guest nice in total Jiffie
         // as they are already part of nice
         if((idx != 9) && (idx != 10))
            totalJiffies = totalJiffies + stol(Jiffie);
         // Get idle Jiffie for computing active Jiffie
         if((idx == 4) || (idx == 5))
         {
            idleJiffies += stol(Jiffie);
         }
       }
       else
       {
         linestream >> Jiffie;
       }
       idx = idx + 1;
    }  
    stream.close();
    return totalJiffies;
  }
  else
  {
    return 0;
  }
}

// TODO: Read and return the number of active jiffies for a PID
// REMOVE: [[maybe_unused]] once you define the function
long LinuxParser::ActiveJiffies(int pid) 
{ 
  
  string line{""},Jiffie{""};
  int    idx = 0;
  long   totJiffies = 0;
  string spid{""};
  std::stringstream sstream;
  sstream << pid;
  sstream >> spid;
  std::ifstream stream(kProcDirectory + spid + kStatFilename);
  // First line of stat files contains system jiffies info
  // adding up all jiffies here
  if (stream.is_open()) 
  {
    std::getline(stream, line);
    std::stringstream linestream(line);
    while (linestream.good())
    {
       //skip first string
       if(idx != 0) 
       {
         linestream >> Jiffie;
         // Donot include Guest and Guest nice in total Jiffie
         // as they are already part of nice
         if((idx == 13) || (idx == 14))// || 
          // (idx == 15) || (idx == 16))
         {
            totJiffies = totJiffies + stol(Jiffie);
         }
       }
       else
       {
         linestream >> Jiffie;
       }
       idx = idx + 1;
    }
    stream.close();
    return (totJiffies/sysconf(_SC_CLK_TCK));
  }
  else
  {
    return 0;
  }
}


// TODO: Read and return the number of active jiffies for the system
long LinuxParser::ActiveJiffies() 
{
  //Jiffies compute total Jiffies, negative idleKiffies to get
  //active Jiffies
  return(LinuxParser::Jiffies() - idleJiffies);
}

// TODO: Read and return the number of idle jiffies for the system
long LinuxParser::IdleJiffies() 
{ 
  LinuxParser::Jiffies();
  return idleJiffies; 
}

// TODO: Read and return CPU utilization
float LinuxParser::CpuUtilization() 
{ 
  //vector<string> cpuutilization = 0;
  string sCpu;
  float   lcpu;
  std::stringstream sstream;
  LinuxParser::Jiffies();
  
  lcpu = ((float)(totalJiffies-idleJiffies)/(float)totalJiffies) ;
  sstream << lcpu;
  sstream >> sCpu;
 // cpuutilization.push_back(sCpu);
  return lcpu; 

}

// TODO: Read and return the total number of processes
int LinuxParser::TotalProcesses() 
{
  string sProc,sProcCount;
  string line;
  std::ifstream filestream(kProcDirectory + kStatFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) 
    {
      std::istringstream linestream(line);
      linestream >> sProc >> sProcCount;
      //Found total processes
      if(sProc == "processes")
      {
        break;
      }
    }
    filestream.close();
  }
  return stoi(sProcCount);
}

// TODO: Read and return the number of running processes
int LinuxParser::RunningProcesses() 
{ 
  string sProc,sProcCount;
  string line;
  std::ifstream filestream(kProcDirectory + kStatFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) 
    {
      std::istringstream linestream(line);
      linestream >> sProc >> sProcCount;
      //Found running processes
      if(sProc == "procs_running")
      {
        break;
      }
    }
    filestream.close();
    return stoi(sProcCount);
  }
  else
  {
    return 0; 
  }
}

// TODO: Read and return the command associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Command(int pid) 
{
  string spid;
  std::stringstream sstream;
  sstream << pid;
  sstream >> spid;
  std::ifstream stream(kProcDirectory + spid + kCmdlineFilename);
  string line;

  if (stream.is_open()) {
    std::getline(stream, line);
    stream.close();
    return line;
  }
  else
  {
    return string();
  }
}

// TODO: Read and return the memory used by a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Ram(int pid) 
{ 
  string spid,sString,sMem,sUnits;
  string line;
  std::stringstream sstream;
  float  MemUsage;
  sstream << pid;
  sstream >> spid;
  std::ifstream filestream(kProcDirectory + spid + kStatusFilename);

  if (filestream.is_open()) {
    while (std::getline(filestream, line)) 
    {
      std::istringstream linestream(line);
      linestream >> sString >> sMem >> sUnits;
      //Found total processes
      if(sString == "VmSize:")
      {
         break;
      }
    }
    filestream.close();
    //Convert to Mb
    MemUsage = stof(sMem)/1000;
    sstream << MemUsage;
    sstream >> sMem;
    return sMem;
  }
  else
  {
    return string();
  }
}

// TODO: Read and return the user ID associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Uid(int pid) 
{ 
  string line,userId,sUsr;
  stringstream sstream;

  string spid;
  sstream << pid;
  sstream >> spid;
  std::ifstream stream(kProcDirectory + spid + kStatusFilename);
  // First line of stat files contains system jiffies info
  // adding up all jiffies here
  if (stream.is_open()) {
    while (std::getline(stream, line)) 
    {
      std::istringstream linestream(line);
      {
         //skip first string
         linestream >> sUsr >> userId ;
         if(sUsr == "Uid:")
         {
             break;
         }
      }
    }
    stream.close();
    return userId;
  }
  else
  {
    return string();
  }
}

// TODO: Read and return the user associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::User(int pid) 
{
  string line,userId,user,perms;
  stringstream sstream;
  string spid;
  sstream << pid;
  sstream >> spid;
  std::ifstream filestream(kPasswordPath);
  // First line of stat files contains system jiffies info
  // adding up all jiffies here
    if (filestream.is_open()) {
    while (std::getline(filestream, line)) 
    {
      std::replace(line.begin(), line.end(), ':', ' ');
      std::stringstream linestream(line);
      {
         //skip first string
         linestream >> user >> perms >> userId;
         if(userId == Uid(pid))
         {
           break;
         }
      }
    }
    filestream.close();
    return user;
  }
  else
  {
    return string();
  }
}

// TODO: Read and return the uptimeof a process
// REM OVE: [[maybe_unused]] once you define the function
long LinuxParser::UpTime(int pid) 
{ 
  string line,Jiffie;
  int    idx = 0;
  long   upTime = 0;
  string spid;
  std::stringstream sstream;
  sstream << pid;
  sstream >> spid;
  std::ifstream stream(kProcDirectory + spid + kStatFilename);
  // First line of stat files contains system jiffies info
  // adding up all jiffies here
  if (stream.is_open()) {
    std::getline(stream, line);
    std::stringstream linestream(line);
    while (linestream.good())
    {
       //skip first string
       if(idx != 0) 
       {
                  
         linestream >> Jiffie;
         
         // Donot include Guest and Guest nice in total Jiffie
         // as they are already part of nice
         if(idx == 21)
         {
           
            upTime = stol(Jiffie);
            
         }
       }
       else
       {
         linestream >> Jiffie;
       }
       idx = idx + 1;
    }  
    stream.close();
    return (upTime/sysconf(_SC_CLK_TCK));
  }
  else
  {
    return 0;
  }
}