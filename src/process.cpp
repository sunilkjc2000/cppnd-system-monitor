#include <unistd.h>
#include <cctype>
#include <sstream>
#include <string>
#include <vector>

#include "process.h"

using std::string;
using std::to_string;
using std::vector;

// TODO: Return this process's ID
int Process::Pid() { return getProcId(); }

// TODO: Return this process's CPU utilization
float Process::CpuUtilization() { return getUtilization(); }

// TODO: Return the command that generated this process
string Process::Command() { return getCommand(); }

// TODO: Return this process's memory utilization
string Process::Ram() { return getRam(); }

// TODO: Return the user (name) that generated this process
string Process::User() { return getUsrName(); }

// TODO: Return the age of this process (in seconds)
long int Process::UpTime() { return getUpTime(); }

// TODO: Overload the "less than" comparison operator for Process objects
// REMOVE: [[maybe_unused]] once you define the function
bool Process::operator<(Process const& a) const 
{
  bool retStat = false;
  if((cpuUtilization < a.cpuUtilization) &&
     (RAM.compare(a.RAM) < 0))
  { 
     retStat = true;
  }
   return retStat; 
}