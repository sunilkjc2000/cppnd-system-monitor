#ifndef PROCESS_H
#define PROCESS_H

#include <string>
/*
Basic class for Process representation
It contains relevant attributes as shown below
*/
class Process {
 public:
  int Pid();                               // TODO: See src/process.cpp
  std::string User();                      // TODO: See src/process.cpp
  std::string Command();                   // TODO: See src/process.cpp
  float CpuUtilization();                  // TODO: See src/process.cpp
  std::string Ram();                       // TODO: See src/process.cpp
  long int UpTime();                       // TODO: See src/process.cpp
  bool operator<(Process const& a) const;  // TODO: See src/process.cpp

  void setProcId(int procId){ processId = procId; return; }
  int getProcId(){ return processId; }
  
  void setCommand(std::string cmd){ sCommand = cmd; return; }
  std::string getCommand(){ return sCommand; }
  
  void setUsrName(std::string usrName){ sUserName = usrName; return; }
  std::string getUsrName(){ return sUserName; }
 
  void setUtilization(float utilization){ cpuUtilization = utilization; return; }
  float getUtilization(){ return cpuUtilization; }

  void setRam(std::string mem){ RAM = mem; return; }
  std::string getRam(){ return RAM; }
  
  void setUpTime(long int uptime){ upTime = uptime; return; }
  long int getUpTime(){ return upTime; }
  // TODO: Declare any necessary private members
 private:
  int         processId;
  std::string sCommand;
  std::string sUserName;
  float       cpuUtilization;
  std::string RAM;
  long int    upTime;
};

#endif